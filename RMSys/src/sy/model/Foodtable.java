package sy.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Foodtable entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "foodtable", catalog = "RMSysDB")
public class Foodtable implements java.io.Serializable {

	// Fields

	private String id;
	private String tableno;
	private String personnum;
	private Date sittime;
	private String dutywatername;

	// Constructors

	/** default constructor */
	public Foodtable() {
	}

	/** full constructor */
	public Foodtable(String id, String tableno, String personnum, Date sittime,
			String dutywatername) {
		this.id = id;
		this.tableno = tableno;
		this.personnum = personnum;
		this.sittime = sittime;
		this.dutywatername = dutywatername;
	}

	// Property accessors
	@Id
	@Column(name = "id", unique = true, nullable = false, length = 40)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tableno", nullable = false, length = 10)
	public String getTableno() {
		return this.tableno;
	}

	public void setTableno(String tableno) {
		this.tableno = tableno;
	}

	@Column(name = "personnum", nullable = false, length = 10)
	public String getPersonnum() {
		return this.personnum;
	}

	public void setPersonnum(String personnum) {
		this.personnum = personnum;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "sittime", nullable = false, length = 10)
	public Date getSittime() {
		return this.sittime;
	}

	public void setSittime(Date sittime) {
		this.sittime = sittime;
	}

	@Column(name = "dutywatername", nullable = false, length = 10)
	public String getDutywatername() {
		return this.dutywatername;
	}

	public void setDutywatername(String dutywatername) {
		this.dutywatername = dutywatername;
	}

}