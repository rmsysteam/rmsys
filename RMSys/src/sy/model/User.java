package sy.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * User entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "user", catalog = "RMSysDB")
public class User implements java.io.Serializable {

	// Fields

	private String id;
	private String email;
	private String password;
	private String name;
	private String sex;
	private String userno;
	private String dutytable;

	// Constructors

	/** default constructor */
	public User() {
	}

	/** full constructor */
	public User(String id, String email, String password, String name,
			String sex, String userno, String dutytable) {
		this.id = id;
		this.email = email;
		this.password = password;
		this.name = name;
		this.sex = sex;
		this.userno = userno;
		this.dutytable = dutytable;
	}

	// Property accessors
	@Id
	@Column(name = "id", unique = true, nullable = false, length = 40)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "email", nullable = false, length = 30)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "password", nullable = false, length = 60)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "name", nullable = false, length = 30)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "sex", nullable = false, length = 1)
	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	@Column(name = "userno", nullable = false, length = 30)
	public String getUserno() {
		return this.userno;
	}

	public void setUserno(String userno) {
		this.userno = userno;
	}

	@Column(name = "dutytable", nullable = false, length = 10)
	public String getDutytable() {
		return this.dutytable;
	}

	public void setDutytable(String dutytable) {
		this.dutytable = dutytable;
	}

}