package sy.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Food entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "food", catalog = "RMSysDB")
public class Food implements java.io.Serializable {

	// Fields

	private String id;
	private String category;
	private String name;
	private String price;
	private String foodnum;
	private String description;
	private Date ordertime;
	private String taste;

	// Constructors

	/** default constructor */
	public Food() {
	}

	/** full constructor */
	public Food(String id, String category, String name, String price,
			String foodnum, String description, Date ordertime, String taste) {
		this.id = id;
		this.category = category;
		this.name = name;
		this.price = price;
		this.foodnum = foodnum;
		this.description = description;
		this.ordertime = ordertime;
		this.taste = taste;
	}

	// Property accessors
	@Id
	@Column(name = "id", unique = true, nullable = false, length = 40)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "category", nullable = false, length = 40)
	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Column(name = "name", nullable = false, length = 40)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "price", nullable = false, length = 10)
	public String getPrice() {
		return this.price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	@Column(name = "foodnum", nullable = false, length = 40)
	public String getFoodnum() {
		return this.foodnum;
	}

	public void setFoodnum(String foodnum) {
		this.foodnum = foodnum;
	}

	@Column(name = "description", nullable = false, length = 40)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "ordertime", nullable = false, length = 10)
	public Date getOrdertime() {
		return this.ordertime;
	}

	public void setOrdertime(Date ordertime) {
		this.ordertime = ordertime;
	}

	@Column(name = "taste", nullable = false, length = 40)
	public String getTaste() {
		return this.taste;
	}

	public void setTaste(String taste) {
		this.taste = taste;
	}

}